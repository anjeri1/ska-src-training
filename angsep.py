#!/usr/bin/python
# angsep.py
# Program to calculate the angular separation between two points
# whose coordinates are given in RA and Dec

import math
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from astropy.wcs import WCS
from astropy.nddata import Cutout2D
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.odr import Model, Data, ODR
from scipy.stats import linregress
import numpy as np
import matplotlib.cm as cm
from scipy import constants
import pandas as pd
from matplotlib import rc
from matplotlib import rcParams
from astropy.cosmology import Planck15
from astropy import units as u
from astropy.wcs.utils import skycoord_to_pixel
from astropy import units as u
from astropy.coordinates import SkyCoord




co = pd.read_csv('/home/anne/SPARCS/idia_SPARCS/antenna.csv')

#c2 = SkyCoord('15h33m28.32s', '+29d11m05.58s')
#c2 = SkyCoord('15h33m28.318s', '+29d11m05.58s')
#c2 = SkyCoord(ra=233.1357, dec=29.1933, frame='icrs', unit='deg')##EN00452 
#c2 = SkyCoord(ra=233.1485, dec=29.3188, frame='icrs', unit='deg')##EN00451
#c2 = SkyCoord(ra=233.1534, dec=29.2498, frame='icrs', unit='deg')##EN00450
#c2 = SkyCoord(ra=233.1643, dec=29.2856, frame='icrs', unit='deg')##EN00449
#c2 = SkyCoord(ra=233.1665, dec=29.2322, frame='icrs', unit='deg')##EN00448
#c2 = SkyCoord(ra=233.1692, dec=29.1303, frame='icrs', unit='deg')##EN00447
#c2 = SkyCoord(ra=233.1710, dec=29.1563, frame='icrs', unit='deg')##EN00446
#c2 = SkyCoord(ra=233.1777, dec=29.1119, frame='icrs', unit='deg')##EN00445
#c2 = SkyCoord(ra=233.1807, dec=29.3622, frame='icrs', unit='deg')##EN00444
#c2 = SkyCoord(ra=233.2245, dec=29.3201, frame='icrs', unit='deg')##EN00439
#c2 = SkyCoord(ra=233.3639, dec=29.3529, frame='icrs', unit='deg')##EN00425
#c2 =SkyCoord(ra=233.3680, dec=29.1849, frame='icrs', unit='deg')##EN00423
#c2 =SkyCoord(ra=233.3754, dec=29.0540, frame='icrs', unit='deg')##EN00422
#c2 =SkyCoord(ra=233.4698, dec=29.3857, frame='icrs', unit='deg')##EN00410
#c2 = SkyCoord(ra=233.5660, dec=29.1018, frame='icrs', unit='deg')##EN00401
#c2 = SkyCoord(ra=233.5791, dec=29.2331, frame='icrs', unit='deg')##EN00400



c= SkyCoord('15h33m27.00s', '+29d12m40.00s')##POINTING CENTRE


"""imfit VLBI Positions  29/11/2021"""
c= SkyCoord('15h34m18.980365s', '+29d13m59.222250s', frame='icrs', unit=('hour','deg'))#imfit402
c= SkyCoord('15h34m05.5351985s', '+29d20m55.5871184s', frame='icrs', unit=('hour','deg'))
c= SkyCoord('15h34m02.205012s', '+29d21m56.501033s', frame='icrs', unit=('hour','deg'))
c= SkyCoord('15h33m54.199372s', '+29d14m31.401795s', frame='icrs', unit=('hour','deg'))
c= SkyCoord('15h33m54.19977s', '+29d14m31.533945s', frame='icrs', unit=('hour','deg'))
c= SkyCoord('15h33m42.379639s', '+29d16m24.71228s', frame='icrs', unit=('hour','deg'))
c= SkyCoord('15h32m59.709024s', '+29d22m24.812918s', frame='icrs', unit=('hour','deg'))
c= SkyCoord('15h32m56.7349022s', '+29d09m43.4549681s', frame='icrs', unit=('hour','deg'))#438
c= SkyCoord('15h32m54.0821768s', '+29d09m40.7620859s', frame='icrs', unit=('hour','deg'))
c= SkyCoord('15h32m42.648866s', '+29d06m42.679740s', frame='icrs', unit=('hour','deg'))


#c2 = SkyCoord(ra=233.5791, dec=29.2331, frame='icrs', unit='deg') ##402
#c2 = SkyCoord(ra=233.5231, dec=29.3488, frame='icrs', unit='deg') ##407
#c2 = SkyCoord(ra=233.5092, dec=29.3657, frame='icrs', unit='deg') ##408
#c2 = SkyCoord(ra=233.4758, dec=29.2421, frame='icrs', unit='deg') ##410
#c2 = SkyCoord(ra=233.4266, dec=29.2735, frame='icrs', unit='deg') ##419
#c2 = SkyCoord(ra=233.2488, dec=29.3735, frame='icrs', unit='deg') ##437
#c2 = SkyCoord(ra=233.2365, dec=29.1618, frame='icrs', unit='deg') ##438
#c2 = SkyCoord(ra=233.2254, dec=29.1613, frame='icrs', unit='deg') ##439
#c2 = SkyCoord(ra=233.1777, dec=29.1119, frame='icrs', unit='deg') ##446

co= pd.read_csv('/home/anne/SPARCS/idia_SPARCS/cataloger/sources/notaper_sources/SPARCS_notaper_catalog.csv')
#co = pd.read_csv('/home/anne/SPARCS/idia_SPARCS/cataloger/sources/taper_sources/SPARCSN_tapered_catalog.csv') 
RA = co.RA
DEC = co.DEC
c2 = SkyCoord(ra=RA[7], dec=DEC[7], frame='icrs', unit='deg') 


sep = c.separation(c2).mas
print(sep)
dra, ddec = c.spherical_offsets_to(c2)
print(dra.mas, ddec.mas)




#####d=c2.to_string('decimal')
###print(d)
###d=c2.to_string('dms')
###print(d)
###d=c2.to_string('hmsdms')
##print(d)



def hms_dms_dd(ra, dec, delimiter=" "):
    """Convert from HMS; DMS to DD.
    
    Examples:
    >>> ra, dec = hms_dms_dd("00h59m59.3s", "-00d00m01.01s")
    >>> ra, dec
    (14.997083333333332, -0.00028055555555555554)
    >>> ra, dec = hms_dms_dd("23 59 59", "+56 00 00")
    >>> ra, dec
    (359.99583333333334, 56.0)
    >>> ra, dec = hms_dms_dd("24:30:00", "+90:00:00")
    >>> ra, dec
    (7.5, 90.0)
    
    """

    try: 
        ra_dd, dec_dd = float(ra), float(dec)
    except ValueError:

        if ":" in ra:
            delimiter = ":"
        elif "h" in ra:
            ra  = ra.replace("h", " ").replace("m", " ").replace("s", " ")
            dec = dec.replace("d", " ").replace("m", " ").replace("s", " ")

        ra, dec = ra.split(delimiter), dec.split(delimiter)

        # RA:
        ra_hours_dd = float(ra[0]) * 15.
        ra_minutes_dd = float(ra[1]) * 15. / 60.
        ra_seconds_dd = float(ra[2]) * 15. / 3600.
        ra_dd = ra_hours_dd + ra_minutes_dd + ra_seconds_dd
        if ra_dd >= 360.:
            ra_dd = abs(ra_dd  - 360.)

        # DEC:
        if "-" in dec[0]:
            dec_dd = float(dec[0]) - (float(dec[1]) / 60.) - (float(dec[2]) / 3600.)
        else:
            dec_dd = float(dec[0]) + (float(dec[1]) / 60.) + (float(dec[2]) / 3600.)
    

    return ra_dd, dec_dd 

ra, dec = hms_dms_dd('15h33m52.752s',  '+29d23m08.52s')
###print(ra, dec)









'''
Sample_size = len(co.D)
d=0
pbcorr=[]
for ii in range(Sample_size):
    pbcorr.append(np.sqrt((math.exp(- 85.57*co.D[ii]**2*sep**2))))
    d+1
print(pbcorr)   
gencal(vis='en004.ms',caltable='pc1.pbcor', caltype='amp', antenna='JB, WB, EF, MC, NT, O8, T6, UR, TR, HH, SV, ZC, BD, IR, SR, PI, DA, KN, DE, CM', parameter=[pbcorr])

import casatools


#!/usr/bin/python
# angsep.py
# Program to calculate the angular separation between two points
# whose coordinates are given in RA and Dec

#print"\n angsep Written by Enno Middelberg 2001\n"

import math
import string
import sys

inp=sys.argv[0:]
del inp[0]
if len(inp)==0:
    print" Program to convert an the angular separation between"
    print" two points in the sky"
    print" Type 'angsep.py RA1 Dec1 RA2 Dec2' to calculate the"
    print" angular separation. All coordinates must be of the"
    print" form hh:mm:ss(.ssssssss) or hh mm ss(.ssssssss)"
    print" (Don't mix!).\n"
    sys.exit()

# Find and replace any ":" and "=" from inputs
newinp=[]
for x in inp:
    newinp.append(string.replace(x, ":", " "))

inp=newinp

# Find and delete alphanumeric entries like "RA" and "DEC"
newline=""
for x in inp:
    newline=newline+" "
    for y in x:
	newline=newline+y

inp=string.split(newline)

newinp=[]
for x in inp:
    try:
	newinp.append(float(x))
    except ValueError:
	pass

inp=newinp

if len(inp)==4:
    ra1 =string.split(inp[0], ":")
    dec1=string.split(inp[1], ":")
    ra2 =string.split(inp[2], ":")
    dec2=string.split(inp[3], ":")
elif len(inp)==12:
    ra1 =inp[0:3]
    dec1=inp[3:6]
    ra2 =inp[6:9]
    dec2=inp[9:12]
else:
    print" Too few or too many parameters."
    sys.exit()

print ra1, dec1, ra2, dec2

# Calculate angular separation of declinations
# Convert them into degrees and calulate the difference

# conversion of right ascension 1:
ra1hh=(float(ra1[0]))*15
ra1mm=(float(ra1[1])/60)*15
ra1ss=(float(ra1[2])/3600)*15

ra1deg=ra1hh+ra1mm+ra1ss
ra1rad=ra1deg*math.pi/180

# conversion of declination 1:
dec1hh=abs(float(dec1[0]))
dec1mm=float(dec1[1])/60
dec1ss=float(dec1[2])/3600

if float(dec1[0]) < 0:
	dec1deg=-1*(dec1hh+dec1mm+dec1ss)
else:
	dec1deg=dec1hh+dec1mm+dec1ss

dec1rad=dec1deg*math.pi/180

# conversion of right ascension 2:
ra2hh=float(ra2[0])*15
ra2mm=(float(ra2[1])/60)*15
ra2ss=(float(ra2[2])/3600)*15

ra2deg=ra2hh+ra2mm+ra2ss
ra2rad=ra2deg*math.pi/180

# conversion of declination 2:
dec2hh=abs(float(dec2[0]))
dec2mm=float(dec2[1])/60
dec2ss=float(dec2[2])/3600

if float(dec2[0]) < 0:
	dec2deg=-1*(dec2hh+dec2mm+dec2ss)
else:
	dec2deg=dec2hh+dec2mm+dec2ss

dec2rad=dec2deg*math.pi/180

# calculate scalar product for determination
# of angular separation

x=math.cos(ra1rad)*math.cos(dec1rad)*math.cos(ra2rad)*math.cos(dec2rad)
y=math.sin(ra1rad)*math.cos(dec1rad)*math.sin(ra2rad)*math.cos(dec2rad)
z=math.sin(dec1rad)*math.sin(dec2rad)

rad=math.acos(x+y+z)

# Delta RA
deg  = abs((ra2rad-ra1rad)*180/math.pi)
deg_corrected=math.cos(dec1rad)*deg
hh   =int(deg/15)
mm   =int((deg-15*hh)*4)
ss   =(4*deg-60*hh-mm)*60
print "\n Delta RA:  "+string.zfill(`hh`,2)+':'+string.zfill(`mm`,2)+':'+'%10.8f, hms format' % ss

hh   =int(deg)
mm   =int((deg-int(deg))*60)
ss   =((deg-int(deg))*60-mm)*60
print " Delta RA:  "+string.zfill(`hh`,2)+':'+string.zfill(`mm`,2)+':'+'%10.8f, dms format' % ss

# Delta RA corrected for declination (dms format)
deg_corrected=math.cos(dec1rad)*deg
hh   =int(deg_corrected)
mm   =int((deg_corrected-int(deg_corrected))*60)
ss   =((deg_corrected-int(deg_corrected))*60-mm)*60
print " Delta RA:  "+string.zfill(`hh`,2)+':'+string.zfill(`mm`,2)+':'+'%10.8f, dms format, corrected with cos(declination 1)' % ss

# Delta DEC
deg = abs((dec1rad-dec2rad)*180/math.pi)
hh   =int(deg)
mm   =int((deg-int(deg))*60)
ss   =((deg-int(deg))*60-mm)*60
print " Delta DEC: "+string.zfill(`hh`,2)+':'+string.zfill(`mm`,2)+':'+'%10.8f (dms format)' % ss

# use Pythargoras approximation if rad < 1 arcsec
if rad<0.000004848:
    print "\n Angular separation < ~1 arcsec, using Pythagorean approximation."
    rad=math.sqrt((math.cos(dec1rad)*(ra1rad-ra2rad))**2+(dec1rad-dec2rad)**2)
    
# Angular separation
deg=rad*180/math.pi
hh=int(deg)
mm=int((deg-int(deg))*60)
ss=((deg-int(deg))*60-mm)*60

print "\n Angular Separation: "+string.zfill(`hh`,2)+":"+string.zfill(`mm`,2)+":"+'%10.8f (dms format)' % ss
print "\n Accuracy currently not better than 3 microarcsec!\n"


'''



















